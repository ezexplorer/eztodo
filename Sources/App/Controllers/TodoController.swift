//
//  TodoController.swift
//  App
//
//  Created by Daniel Moro on 24.7.17..
//

import Foundation
import Vapor
import HTTP

final class TodoController {
    var droplet : Droplet
    
    init(droplet: Droplet) {
        self.droplet = droplet
    }
    
    fileprivate func makeMessage(owner_name: String, assignee: String?, task: String?, date: Date?, responseURL: String) throws {
        let client = droplet.client
        let headers : [HeaderKey: String] = [HeaderKey.contentType: "application/json"]
        //create sample message
        let msg = Message(delete_original: false, response_type: "in_channel", text: "I will create ToDo Item with following parameters:", thread_ts: nil, replace_original: false)
        try msg.save()
        
        let att_owner = Attachment(fallback: "Task", author_link: nil, thumb_url: nil, pretext: nil, author_name: nil, ts: 0, color: nil, attachment_type: "defalut", author_icon: nil, text: owner_name, callback_id: "msg_owner", title: "Owner", image_url: nil, messageIdentifier: msg.id)
        try att_owner.save()
        
        if let assignee = assignee {
            let f_assignee = Field(title: "Who", short: true, value: assignee, attachmentIdentifier: att_owner.id)
            try f_assignee.save()
        }
        if  let task = task, task.characters.count > 0 {
            let f_task = Field(title: "What", short: false, value: task, attachmentIdentifier: att_owner.id)
            try f_task.save()
        }
        if let date = date {
            let f_date = Field(title: "When", short: true, value: DateFormatter.localizedString(from: date, dateStyle: .medium, timeStyle: .none), attachmentIdentifier: att_owner.id)
            try f_date.save()
        }
        
        let att_patient = Attachment(fallback: "Patient", author_link: nil, thumb_url: nil, pretext: nil, author_name: nil, ts: nil, color: nil, attachment_type: "defalut", author_icon: nil, text: "If this todo is regarding a patient, please choose which one:", callback_id: "msg_patient", title: "Patient", image_url: nil, messageIdentifier: msg.id)
        try att_patient.save()
        
        let action_patient = Action(name: "Select Patient", value: "search_patient", type: Action.ActionType.menu, text: "Patients", data_source: Action.DataSource.external, attachmentIdentifier: att_patient.id)
        try action_patient.save()
        
        let json = try msg.makeJSON()
        
        _ = try client.post(responseURL, headers, json)
    }
    
    fileprivate func extractFromRawMessage(text: String) -> (date: Date?, task: String?, assignee: String?) {
        //let types: NSTextCheckingResult.CheckingType = [.date]
        var date : Date?
        var task : String = text
//        if let dateDetector = try? NSDataDetector(types: types.rawValue) {
//            if let result = dateDetector.firstMatch(in: text, options: [], range: NSRange.init(location: 0, length: text.characters.count)) {
//                date = result.date
//                if let r2 = text.range(from: result.range) {
//                    task.replaceSubrange(r2, with: "")
//                }
//            }
//        }
        var assignee : String?
        if let tokenExpression = try? NSRegularExpression(pattern: "<.*>", options: []) {
            let matches = tokenExpression.matches(in: text, options: [], range: NSRange.init(location: 0, length: text.characters.count))
            for match in matches {
                if let r2 = text.range(from: match.range) {
                    var subString = text.substring(with: r2)
                    task.replaceSubrange(r2, with: "")
                    subString = subString.trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
                    var array = subString.components(separatedBy: "|")
                    if array.count > 1 {
                        assignee = array[1]
                    }
                }
            }
        }
        
        return (date: date, task: task, assignee: assignee)
    }
    
    func digestTodo(request: Request) throws -> ResponseRepresentable {
        if let params = request.formURLEncoded  {
            let responseURL: String = try params.get("response_url")
            let owner_name : String = try params.get("user_name")
            let raw_text : String = try params.get("text")
            let extracted = extractFromRawMessage(text: raw_text)
            try makeMessage(owner_name: owner_name, assignee: extracted.assignee, task: extracted.task, date: extracted.date, responseURL: responseURL)
        }
        return Response(status: .ok)
    }
    
    func makeRoutes(basePath: RouteBuilder) {
        basePath.post("todo", handler: digestTodo)
    }
}
