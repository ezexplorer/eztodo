//
//  MessageController.swift
//  App
//
//  Created by Daniel Moro on 27.7.17..
//

import Foundation
import Vapor
import HTTP

final class MessageController {
    var droplet : Droplet
    
    init(droplet: Droplet) {
        self.droplet = droplet
    }
    
    func makeRoutes(basePath: RouteBuilder) {
        let massagePath = basePath.grouped("messages")
        massagePath.post("sources", handler: digestSources)
        massagePath.post("actions", handler: digestActions)
    }
    
    let patients = [["text":"Barbara Thompson","value":"1234"],["text":"Barbara Thompson2","value":"5678"],["text":"Maria Johanson","value":"5679"],["text":"Robert Vain","value":"5620"],["text":"Nocole Beagle","value":"4678"],["text":"Charlotte Bonet","value":"6678"],["text":"Bronte Campbel","value":"5778"]]
    
    func digestSources(request: Request) throws -> ResponseRepresentable {
        
        
        var filteredPatients = patients

        if let params = request.formURLEncoded  {
            let jsonString : String = try params.get("payload")
            let data = jsonString.data(using: .utf8, allowLossyConversion: true)
            let bytes = data?.makeBytes()
            if let json = try? JSON(bytes: bytes!) {
                let id : String = try json.get("callback_id")
                let actionName : String = try json.get("name")
                let value : String = try json.get("value")
                switch id {
                case "msg_patient" :
                    //message
                    switch actionName {
                    case "Select Patient":
                        if value.characters.count > 0 {
                            filteredPatients = filteredPatients.filter{($0["text"]?.contains(value))!}
                        }
                    default: break
                    }
                default: break
                }
            }
        }
        
        
        var json = JSON()
        try json.set("options", filteredPatients)
        return json
    }
    
    fileprivate func makeMessage(patient_name: String, responseURL: String) throws {
        let client = droplet.client
        let headers : [HeaderKey: String] = [HeaderKey.contentType: "application/json"]
        //create sample message
        let msg = Message(delete_original: false, response_type: "in_channel", text: "OK, this todo is regarding:", thread_ts: nil, replace_original: false)
        try msg.save()

        
        let att_patient = Attachment(fallback: "Patient", author_link: nil, thumb_url: nil, pretext: nil, author_name: nil, ts: 0, color: nil, attachment_type: "defalut", author_icon: nil, text: patient_name, callback_id: "msg_patient", title: "Patient", image_url: nil, messageIdentifier: msg.id)
        try att_patient.save()
        
        let action_patient = Action(name: "Remove Patient", value: "remove_patient", type: Action.ActionType.button, text: "Remove", data_source: nil, attachmentIdentifier: att_patient.id)
        try action_patient.save()

        let json = try msg.makeJSON()
        
        _ = try client.post(responseURL, headers, json)
    }
    
    fileprivate func extractFromRawMessage(text: String) -> (date: Date?, task: String?, assignee: String?) {
 //       let types: NSTextCheckingResult.CheckingType = [.date]
        var date : Date?
//        if let dateDetector = try? NSDataDetector(types: types.rawValue) {
//            if let result = dateDetector.firstMatch(in: text, options: [], range: NSRange.init(location: 0, length: text.characters.count)) {
//                date = result.date
//            }
//        }
        var assignee : String?
        var task : String = text
        if let tokenExpression = try? NSRegularExpression(pattern: "<.*>", options: []) {
            let matches = tokenExpression.matches(in: text, options: [], range: NSRange.init(location: 0, length: text.characters.count))
            for match in matches {
                if let r2 = text.range(from: match.range) {
                    var subString = text.substring(with: r2)
                    task.replaceSubrange(r2, with: "")
                    subString = subString.trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
                    var array = subString.components(separatedBy: "|")
                    if array.count > 1 {
                        assignee = array[1]
                    }
                }
            }
        }
        
        return (date: date, task: task, assignee: assignee)
    }
    
    func digestActions(request: Request) throws -> ResponseRepresentable {
        if let params = request.formURLEncoded  {
            let jsonString : String = try params.get("payload")
            
            if  let data = jsonString.data(using: .utf8, allowLossyConversion: true),
                let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any] {
                if let id = json["callback_id"] as? String {
                    switch id {
                    case "msg_patient" :
                        //message
                        if  let actions = json["actions"] as? [[String: Any]],
                            let responseURL = json["response_url"] as? String {
                            
                            if let action = actions.first {
                                if let name = action["name"] as? String, name == "Select Patient" {
                                    if let selection = action["selected_options"] as? [[String:String]] {
                                        if let first = selection.first {
                                            if let value = first["value"] {
                                                let selectedPatient = patients.filter{$0["value"]?.lowercased() == value.lowercased()}.first
                                                if let patientName = selectedPatient?["text"] {
                                                    try makeMessage(patient_name: patientName, responseURL: responseURL)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    default: break
                    }
                }
            }
        }
        return Response(status: .ok)
    }
}
