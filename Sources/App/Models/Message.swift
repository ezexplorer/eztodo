import Vapor
import FluentProvider

final class Message : Model  {
    static let idKey = "id"
    static let foreignIdKey = "message_id"
    
    struct Properties {
        static let delete_original = "delete_original"
        static let response_type = "response_type"
        static let id = "id"
        static let text = "text"
        static let thread_ts = "thread_ts"
        static let replace_original = "replace_original"
    }
    
    let storage = Storage()
    
    var delete_original: Bool?
    var response_type: String?
    var text: String?
    var thread_ts: String?
    var replace_original: Bool?
    
    init(delete_original: Bool?, response_type: String?, text: String?, thread_ts: String?, replace_original: Bool?) {
        self.delete_original = delete_original
        self.response_type = response_type
        self.text = text
        self.thread_ts = thread_ts
        self.replace_original = replace_original
    }
    
    init(row: Row) throws {
        delete_original = try row.get(Properties.delete_original)
        response_type = try row.get(Properties.response_type)
        id = try row.get(Properties.id)
        text = try row.get(Properties.text)
        thread_ts = try row.get(Properties.thread_ts)
        replace_original = try row.get(Properties.replace_original)
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set(Properties.delete_original, delete_original)
        try row.set(Properties.response_type, response_type)
        try row.set(Properties.id, id)
        try row.set(Properties.text, text)
        try row.set(Properties.thread_ts, thread_ts)
        try row.set(Properties.replace_original, replace_original)
        return row
    }
}
    
extension Message: Preparation {
    static func prepare(_ database: Database) throws {
        try database.create(self) { messages in
            messages.id()
            messages.bool(Properties.delete_original, optional: true)
            messages.string(Properties.response_type, optional: true)
            messages.string(Properties.text, optional: true)
            messages.string(Properties.thread_ts, optional: true)
            messages.bool(Properties.replace_original, optional: true)
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
    
extension Message: ResponseRepresentable { }
    
extension Message: JSONConvertible {
    convenience init(json: JSON) throws {
        try self.init(
            delete_original: json.get(Properties.delete_original),
            response_type: json.get(Properties.response_type),
            text: json.get(Properties.text),
            thread_ts: json.get(Properties.thread_ts),
            replace_original: json.get(Properties.replace_original)
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("className", "Message")
        try json.set(Properties.delete_original, delete_original)
        try json.set(Properties.response_type, response_type)
        try json.set(Properties.id, id)
        try json.set(Properties.text, text)
        try json.set(Properties.thread_ts, thread_ts)
        try json.set(Properties.replace_original, replace_original)
        //attachments
        let c_attachments = try attachments.all()
        try json.set("attachments", c_attachments)
        return json
    }
}
    
extension Message: JSONUpdateable {
    func update(json: JSON) throws {
        self.delete_original = try json.get(Properties.delete_original)
        self.response_type = try json.get(Properties.response_type)
        self.text = try json.get(Properties.text)
        self.thread_ts = try json.get(Properties.thread_ts)
        self.replace_original = try json.get(Properties.replace_original)
    }
    
    static func makeOrUpdate(json: JSON) throws -> Message {
        guard let id : Identifier = try json.get(Properties.id), let model = try self.find(id) else  {
            return try Message(json: json)
        }
        
        try model.update(json: json)
        return model
    }
}
    
extension Message {
    var attachments: Children<Message, Attachment> {
        return children()
    }

}
