//
//  Todo.swift
//  App
//
//  Created by Daniel Moro on 24.7.17..
//

import FluentProvider

final class Todo: Model {
    let storage = Storage()
    
    fileprivate enum CodingKeys : String {
        case title
    }
    
    //properties
    var title: String?
    
    init(title: String?) {
        self.title = title
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        
        try row.set(CodingKeys.title.rawValue, title)
        
        return row
    }
    
    init(row: Row) throws {
        title = try row.get(CodingKeys.title.rawValue)
    }
    
}

extension Todo: Preparation {
    static func prepare(_ database: Database) throws {
        try database.create(self) { devices in
            devices.id()
            devices.string(CodingKeys.title.rawValue, optional: true)
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

extension Todo: JSONConvertible {
    func makeJSON() throws -> JSON {
        var json = JSON()
        
        try json.set(CodingKeys.title.rawValue, title)
        
        return json
    }
    
    convenience init(json: JSON) throws {
        try self.init(
            title: json.get(CodingKeys.title.rawValue)
        )
    }
}

extension Todo: ResponseRepresentable { }
