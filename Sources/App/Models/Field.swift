import Vapor
import FluentProvider

final class Field : Model  {
    static let idKey = "id"
    static let foreignIdKey = "field_id"
    
    struct Properties {
        static let title = "title"
        static let short = "short"
        static let id = "id"
        static let value = "value"
        static let attachmentIdentifier = "attachment_id"
    }
    
    let storage = Storage()
    
    var title: String?
    var short: Bool?
    var value: String?
    var attachmentIdentifier: Identifier?
    
    init(title: String?, short: Bool?, value: String?, attachmentIdentifier: Identifier?) {
        self.title = title
        self.short = short
        self.value = value
        self.attachmentIdentifier = attachmentIdentifier
    }
    
    init(row: Row) throws {
        title = try row.get(Properties.title)
        short = try row.get(Properties.short)
        id = try row.get(Properties.id)
        value = try row.get(Properties.value)
        attachmentIdentifier = try row.get(Properties.attachmentIdentifier)
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set(Properties.title, title)
        try row.set(Properties.short, short)
        try row.set(Properties.id, id)
        try row.set(Properties.value, value)
        try row.set(Properties.attachmentIdentifier, attachmentIdentifier)
        return row
    }
}
    
extension Field: Preparation {
    static func prepare(_ database: Database) throws {
        try database.create(self) { fields in
            fields.id()
            fields.string(Properties.title, optional: true)
            fields.bool(Properties.short, optional: true)
            fields.string(Properties.value, optional: true)
            fields.parent(Attachment.self, optional: true, unique: false, foreignIdKey: Properties.attachmentIdentifier)
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
    
extension Field: ResponseRepresentable { }
    
extension Field: JSONConvertible {
    convenience init(json: JSON) throws {
        try self.init(
            title: json.get(Properties.title),
            short: json.get(Properties.short),
            value: json.get(Properties.value),
            attachmentIdentifier: json.get(Properties.attachmentIdentifier)
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("className", "Field")
        try json.set(Properties.title, title)
        try json.set(Properties.short, short)
        try json.set(Properties.id, id)
        try json.set(Properties.value, value)
        try json.set(Properties.attachmentIdentifier, attachmentIdentifier)
        return json
    }
}
    
extension Field: JSONUpdateable {
    func update(json: JSON) throws {
        self.title = try json.get(Properties.title)
        self.short = try json.get(Properties.short)
        self.value = try json.get(Properties.value)
    }
    
    static func makeOrUpdate(json: JSON) throws -> Field {
        guard let id : Identifier = try json.get(Properties.id), let model = try self.find(id) else  {
            return try Field(json: json)
        }
        
        try model.update(json: json)
        return model
    }
}
    
extension Field {
    var attachment: Parent<Field, Attachment> {
        return parent(id: attachmentIdentifier)
    }

}
