import Vapor
import FluentProvider

final class Action : Model  {
    
    enum ActionType : String {
        case button = "button"
        case menu = "select"
    }
    
    enum DataSource: String {
        case users
        case channels
        case external
    }
    
    static let idKey = "id"
    static let foreignIdKey = "action_id"
    
    struct Properties {
        static let name = "name"
        static let value = "value"
        static let id = "id"
        static let type = "type"
        static let text = "text"
        static let data_source = "data_source"
        static let attachmentIdentifier = "attachment_id"
    }
    
    let storage = Storage()
    
    var name: String?
    var value: String?
    var type: ActionType?
    var text: String?
    var data_source: DataSource?
    var attachmentIdentifier: Identifier?
    
    init(name: String?, value: String?, type: ActionType?, text: String?, data_source: DataSource?, attachmentIdentifier: Identifier?) {
        self.name = name
        self.value = value
        self.type = type
        self.text = text
        self.data_source = data_source
        self.attachmentIdentifier = attachmentIdentifier
    }
    
    init(row: Row) throws {
        name = try row.get(Properties.name)
        value = try row.get(Properties.value)
        id = try row.get(Properties.id)
        if let t = try? row.get(Properties.type) as String {
            type = ActionType(rawValue: t)
        }
        text = try row.get(Properties.text)
        if let ds = try? row.get(Properties.data_source) as String {
            data_source = DataSource(rawValue: ds)
        }
        attachmentIdentifier = try row.get(Properties.attachmentIdentifier)
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set(Properties.name, name)
        try row.set(Properties.value, value)
        try row.set(Properties.id, id)
        try row.set(Properties.type, type?.rawValue)
        try row.set(Properties.text, text)
        try row.set(Properties.data_source, data_source?.rawValue)
        try row.set(Properties.attachmentIdentifier, attachmentIdentifier)
        return row
    }
}
    
extension Action: Preparation {
    static func prepare(_ database: Database) throws {
        try database.create(self) { actions in
            actions.id()
            actions.string(Properties.name, optional: true)
            actions.string(Properties.value, optional: true)
            actions.string(Properties.type, optional: true)
            actions.string(Properties.text, optional: true)
            actions.string(Properties.data_source, optional: true)
            actions.parent(Attachment.self, optional: true, unique: false, foreignIdKey: Properties.attachmentIdentifier)
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
    
extension Action: ResponseRepresentable { }
    
extension Action: JSONConvertible {
    convenience init(json: JSON) throws {
        try self.init(
            name: json.get(Properties.name),
            value: json.get(Properties.value),
            type: ActionType(rawValue: json.get(Properties.type)),
            text: json.get(Properties.text),
            data_source: DataSource(rawValue:json.get(Properties.data_source)),
            attachmentIdentifier: json.get(Properties.attachmentIdentifier)
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("className", "Action")
        try json.set(Properties.name, name)
        try json.set(Properties.value, value)
        try json.set(Properties.id, id)
        try json.set(Properties.type, type?.rawValue)
        try json.set(Properties.text, text)
        try json.set(Properties.data_source, data_source?.rawValue)
        try json.set(Properties.attachmentIdentifier, attachmentIdentifier)
        return json
    }
}
    
extension Action: JSONUpdateable {
    func update(json: JSON) throws {
        self.name = try json.get(Properties.name)
        self.value = try json.get(Properties.value)
        self.type = try ActionType(rawValue: json.get(Properties.type))
        self.text = try json.get(Properties.text)
        self.data_source = DataSource(rawValue: try json.get(Properties.data_source))
    }
    
    static func makeOrUpdate(json: JSON) throws -> Action {
        guard let id : Identifier = try json.get(Properties.id), let model = try self.find(id) else  {
            return try Action(json: json)
        }
        
        try model.update(json: json)
        return model
    }
}
    
extension Action {
    var attachment: Parent<Action, Attachment> {
        return parent(id: attachmentIdentifier)
    }

}
