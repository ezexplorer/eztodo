import Vapor
import FluentProvider

final class Attachment : Model  {
    static let idKey = "id"
    static let foreignIdKey = "attachment_id"
    
    struct Properties {
        static let author_link = "author_link"
        static let thumb_url = "thumb_url"
        static let pretext = "pretext"
        static let author_name = "author_name"
        static let ts = "ts"
        static let color = "color"
        static let attachment_type = "attachment_type"
        static let author_icon = "author_icon"
        static let id = "id"
        static let text = "text"
        static let callback_id = "callback_id"
        static let title = "title"
        static let image_url = "image_url"
        static let fallback = "fallback"
        static let messageIdentifier = "message_id"
    }
    
    let storage = Storage()
    
    var author_link: String?
    var thumb_url: String?
    var pretext: String?
    var author_name: String?
    var ts: Double?
    var color: String?
    var attachment_type: String?
    var author_icon: String?
    var text: String?
    var callback_id: String?
    var title: String?
    var image_url: String?
    var fallback: String
    var messageIdentifier: Identifier?
    
    init(fallback: String, author_link: String?, thumb_url: String?, pretext: String?, author_name: String?, ts: Double?, color: String?, attachment_type: String?, author_icon: String?, text: String?, callback_id: String?, title: String?, image_url: String?, messageIdentifier: Identifier?) {
        self.fallback = fallback
        self.author_link = author_link
        self.thumb_url = thumb_url
        self.pretext = pretext
        self.author_name = author_name
        self.ts = ts
        self.color = color
        self.attachment_type = attachment_type
        self.author_icon = author_icon
        self.text = text
        self.callback_id = callback_id
        self.title = title
        self.image_url = image_url
        self.messageIdentifier = messageIdentifier
    }
    
    init(row: Row) throws {
        fallback = try row.get(Properties.fallback)
        author_link = try row.get(Properties.author_link)
        thumb_url = try row.get(Properties.thumb_url)
        pretext = try row.get(Properties.pretext)
        author_name = try row.get(Properties.author_name)
        ts = try row.get(Properties.ts)
        color = try row.get(Properties.color)
        attachment_type = try row.get(Properties.attachment_type)
        author_icon = try row.get(Properties.author_icon)
        id = try row.get(Properties.id)
        text = try row.get(Properties.text)
        callback_id = try row.get(Properties.callback_id)
        title = try row.get(Properties.title)
        image_url = try row.get(Properties.image_url)
        messageIdentifier = try row.get(Properties.messageIdentifier)
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set(Properties.fallback, fallback)
        try row.set(Properties.author_link, author_link)
        try row.set(Properties.thumb_url, thumb_url)
        try row.set(Properties.pretext, pretext)
        try row.set(Properties.author_name, author_name)
        try row.set(Properties.ts, ts)
        try row.set(Properties.color, color)
        try row.set(Properties.attachment_type, attachment_type)
        try row.set(Properties.author_icon, author_icon)
        try row.set(Properties.id, id)
        try row.set(Properties.text, text)
        try row.set(Properties.callback_id, callback_id)
        try row.set(Properties.title, title)
        try row.set(Properties.image_url, image_url)
        try row.set(Properties.messageIdentifier, messageIdentifier)
        return row
    }
}
    
extension Attachment: Preparation {
    static func prepare(_ database: Database) throws {
        try database.create(self) { attachments in
            attachments.id()
            attachments.string(Properties.fallback, optional: false)
            attachments.string(Properties.author_link, optional: true)
            attachments.string(Properties.thumb_url, optional: true)
            attachments.string(Properties.pretext, optional: true)
            attachments.string(Properties.author_name, optional: true)
            attachments.double(Properties.ts, optional: true)
            attachments.string(Properties.color, optional: true)
            attachments.string(Properties.attachment_type, optional: true)
            attachments.string(Properties.author_icon, optional: true)
            attachments.string(Properties.text, optional: true)
            attachments.string(Properties.callback_id, optional: true)
            attachments.string(Properties.title, optional: true)
            attachments.string(Properties.image_url, optional: true)
            attachments.parent(Message.self, optional: true, unique: false, foreignIdKey: Properties.messageIdentifier)
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
    
extension Attachment: ResponseRepresentable { }
    
extension Attachment: JSONConvertible {
    convenience init(json: JSON) throws {
        try self.init(
            fallback: json.get(Properties.fallback),
            author_link: json.get(Properties.author_link),
            thumb_url: json.get(Properties.thumb_url),
            pretext: json.get(Properties.pretext),
            author_name: json.get(Properties.author_name),
            ts: json.get(Properties.ts),
            color: json.get(Properties.color),
            attachment_type: json.get(Properties.attachment_type),
            author_icon: json.get(Properties.author_icon),
            text: json.get(Properties.text),
            callback_id: json.get(Properties.callback_id),
            title: json.get(Properties.title),
            image_url: json.get(Properties.image_url),
            messageIdentifier: json.get(Properties.messageIdentifier)
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set(Properties.fallback, fallback)
        try json.set("className", "Attachment")
        try json.set(Properties.author_link, author_link)
        try json.set(Properties.thumb_url, thumb_url)
        try json.set(Properties.pretext, pretext)
        try json.set(Properties.author_name, author_name)
        try json.set(Properties.ts, ts)
        try json.set(Properties.color, color)
        try json.set(Properties.attachment_type, attachment_type)
        try json.set(Properties.author_icon, author_icon)
        try json.set(Properties.id, id)
        try json.set(Properties.text, text)
        try json.set(Properties.callback_id, callback_id)
        try json.set(Properties.title, title)
        try json.set(Properties.image_url, image_url)
        //fields
        let c_fields = try fields.all()
        try json.set("fields", c_fields)
        //actions
        let c_actions = try actions.all()
        try json.set("actions", c_actions)
        try json.set(Properties.messageIdentifier, messageIdentifier)
        return json
    }
}
    
extension Attachment: JSONUpdateable {
    func update(json: JSON) throws {
        self.fallback = try json.get(Properties.fallback)
        self.author_link = try json.get(Properties.author_link)
        self.thumb_url = try json.get(Properties.thumb_url)
        self.pretext = try json.get(Properties.pretext)
        self.author_name = try json.get(Properties.author_name)
        self.ts = try json.get(Properties.ts)
        self.color = try json.get(Properties.color)
        self.attachment_type = try json.get(Properties.attachment_type)
        self.author_icon = try json.get(Properties.author_icon)
        self.text = try json.get(Properties.text)
        self.callback_id = try json.get(Properties.callback_id)
        self.title = try json.get(Properties.title)
        self.image_url = try json.get(Properties.image_url)
    }
    
    static func makeOrUpdate(json: JSON) throws -> Attachment {
        guard let id : Identifier = try json.get(Properties.id), let model = try self.find(id) else  {
            return try Attachment(json: json)
        }
        
        try model.update(json: json)
        return model
    }
}
    
extension Attachment {
    var fields: Children<Attachment, Field> {
        return children()
    }

    var actions: Children<Attachment, Action> {
        return children()
    }

    var message: Parent<Attachment, Message> {
        return parent(id: messageIdentifier)
    }

}
